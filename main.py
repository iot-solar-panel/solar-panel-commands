import logging
import signal

import scan_com
from solar_panel_controller.solar_panel_controller import SolarPanelController
from solar_panel_controller.solar_panel_keyboard import SolarPanelKeyboard

logging.basicConfig()
logging.root.setLevel(logging.INFO)


if __name__ == '__main__':
    port_name = scan_com.find_usb_uart()
    if port_name is None:
        logging.root.error(f"port was not found")
        exit(1)

    controller = SolarPanelController(port_name)
    keyboard_controller = SolarPanelKeyboard(controller, angle_increment=1)

    def finalize(*args, **kwargs):
        print("\nexecution finished, finalizing")
        controller.join()

    signal.signal(signal.SIGINT, finalize)

    controller.start()
    keyboard_controller.mainloop()
    finalize()
