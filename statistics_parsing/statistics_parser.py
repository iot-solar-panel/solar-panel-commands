import logging

from .solar_panel_record import SolarPanelRecord
from .power_converter import get_power, get_voltage

log = logging.getLogger(__name__)


class StatisticsParser:

    line_separator = b'\n'

    def __init__(self):
        self._results: list[SolarPanelRecord] = []
        self._buffer: bytes = b''

    def parse(self):
        record = self._parse_next_record()
        while record is not None:
            log.debug(record)
            self._results.append(record)
            record = self._parse_next_record()

    def add_data(self, data):
        self._buffer += data

    def _parse_next_record(self) -> SolarPanelRecord:
        if len(self._buffer) == 0:
            return None

        if self.line_separator in self._buffer:
            line, remaining_data = self._buffer.split(self.line_separator, maxsplit=1)
            self._buffer = remaining_data
            return self._parse_record(line)

        return None

    @staticmethod
    def _parse_record(data: bytes):
        record = SolarPanelRecord.from_bytes(data)
        voltage_units = record.voltage
        record.voltage = get_voltage(voltage_units)
        record.power = get_power(voltage_units)
        return record

    def pop_results(self):
        results = list(self._results)
        self._results = []
        return results

    def ready(self):
        return len(self._results) > 0
