from dataclasses import dataclass


@dataclass
class SolarPanelRecord:
    azimuth: int
    altitude: int
    voltage: float
    power: float = 0

    @classmethod
    def from_bytes(cls, data):
        try:
            values = data.strip(b' \n\r').split(b',')
            if len(values) != 3: raise ValueError()
            ints = map(int, values)
            return SolarPanelRecord(*ints)

        except ValueError:
            raise RuntimeError(f"cannot parse {data}")

    def to_tuple(self):
        return self.azimuth, self.altitude, self.voltage, self.power
