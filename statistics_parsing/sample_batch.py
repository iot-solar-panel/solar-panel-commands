import csv
from dataclasses import dataclass, field
from pathlib import Path

from statistics_parsing.solar_panel_record import SolarPanelRecord


@dataclass
class SampleBatch:
    timestamp: float = 0
    records: list[SolarPanelRecord] = field(default_factory=list)

    def add_record(self, record):
        self.records.append(record)

    @staticmethod
    def from_file(file_path: Path):
        batches: dict[str, SampleBatch] = {}

        with file_path.open("r") as f:
            reader = csv.reader(f, dialect="excel")
            for row in reader:
                time, az, al, voltage, power = row
                if time not in batches: batches[time] = SampleBatch(float(time))
                record = SolarPanelRecord(int(az), int(al), float(voltage), float(power))
                batches[time].records.append(record)

        return list(batches.values())
