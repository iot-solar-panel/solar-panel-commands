import device_specifications as specs


def get_power(_units):
    return get_voltage(_units) ** 2 / specs.load_resistance


def get_voltage(_units):
    return _units * specs.voltage_per_unit


if __name__ == '__main__':
    units = 200
    print(f"voltage: {get_voltage(units)}\n"
          f"power: {get_power(units)}")
