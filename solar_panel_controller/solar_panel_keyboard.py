import logging

from .solar_panel_controller import SolarPanelController
from .keyboard_handler.keyboard_handler import KeyboardHandler

log = logging.getLogger(__name__)


class SolarPanelKeyboard(KeyboardHandler):

    def __init__(self, controller: SolarPanelController, angle_increment=5):
        super().__init__()
        self._controller = controller
        self._angle_increment = angle_increment
        self._read_pin = 0

    def on_up_pressed(self):
        self._controller.add_altitude(self._angle_increment)

    def on_down_pressed(self):
        self._controller.add_altitude(-self._angle_increment)

    def on_left_pressed(self):
        self._controller.add_azimuth(-self._angle_increment)

    def on_right_pressed(self):
        self._controller.add_azimuth(self._angle_increment)

    def on_space_pressed(self):
        self._controller.send(self._read_pin)

    def on_button_pressed(self, key):
        if key == self.get_key('r'):
            self._controller.reset()
        if key == self.get_key('z'):
            log.debug(f"y inverted")
            self._controller.invert_altitude()
        if key == self.get_key('x'):
            log.debug(f"x inverted")
            self._controller.invert_azimuth()
