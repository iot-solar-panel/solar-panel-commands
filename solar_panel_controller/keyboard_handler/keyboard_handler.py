import logging
from abc import abstractmethod, ABCMeta
from functools import cache

from pynput import keyboard
from pynput.keyboard import KeyCode, Key

from .button_click_handler import ButtonClickHandler

log = logging.getLogger(__name__)


class KeyboardHandler(metaclass=ABCMeta):

    exit_key = keyboard.KeyCode.from_char('q')

    def __init__(self):
        self._buttons = {
            self.exit_key: ButtonClickHandler(self.on_finish),
            KeyCode.from_char('w'): ButtonClickHandler(self.on_up_pressed),
            KeyCode.from_char('s'): ButtonClickHandler(self.on_down_pressed),
            KeyCode.from_char('a'): ButtonClickHandler(self.on_left_pressed),
            KeyCode.from_char('d'): ButtonClickHandler(self.on_right_pressed),
            Key.space: ButtonClickHandler(self.on_space_pressed),
        }

        self._listener = keyboard.Listener(on_press=self.on_press,
                                           on_release=self.on_release)

    @abstractmethod
    def on_up_pressed(self):
        pass

    @abstractmethod
    def on_down_pressed(self):
        pass

    @abstractmethod
    def on_left_pressed(self):
        pass

    @abstractmethod
    def on_right_pressed(self):
        pass

    @abstractmethod
    def on_space_pressed(self):
        pass

    def on_button_pressed(self, char):
        pass

    def on_finish(self):
        self._listener.stop()

    def on_press(self, key):
        print('\b', end='')
        log.debug(f"key pressed {key}")
        if key not in self._buttons:
            self.on_button_pressed(key)
            return True

        key_handler = self._buttons[key]
        key_handler.press()
        key_handler.on_event()
        return True

    def on_release(self, key):
        if key in self._buttons:
            self._buttons[key].release()
        return True

    def mainloop(self):
        print("Press command buttons>")
        self._listener.start()
        self._listener.join()

    @staticmethod
    @cache
    def get_key(char):
        try:
            return KeyCode.from_char(char)
        except:
            return None
