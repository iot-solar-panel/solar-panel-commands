import logging
import time
from time import time as now

log = logging.getLogger(__name__)


class ButtonClickHandler:

    repeat_interval = 0.1

    def __init__(self, function):
        self._handler = function
        self._last_repeat_time = now()
        self._is_pressed = False

    def on_event(self):
        self._handler()

    def press(self):
        self._last_repeat_time = now()
        self._is_pressed = True

    def release(self):
        self._is_pressed = False

    def is_pressed(self):
        return self._is_pressed

