import logging
import threading
import queue
import time
from pathlib import Path

import serial

from solar_panel_controller.statistics_listener import StatisticsListener

log = logging.getLogger(__name__)


class SolarPanelController(threading.Thread):

    def __init__(self, port_name):
        super().__init__()
        self._command_queue = queue.Queue()
        self._port_name = port_name
        self._port = None
        self._statistics_listener = StatisticsListener(Path("../data"))
        self._is_running = False

    def start(self):
        self._is_running = True
        self._port = serial.Serial(self._port_name, baudrate=9600, timeout=1)
        self._statistics_listener.set_port(self._port)
        self._statistics_listener.start()
        super().start()

    def join(self, **kwargs):
        self._is_running = False
        super().join(**kwargs)
        self._statistics_listener.join(**kwargs)
        self._port.close()

    def run(self) -> None:
        if self._port is None:
            log.error("port was not set before listening started")

        def _send_next_command():
            command = self._command_queue.get(block=True, timeout=0.1)

            # if execution finished while waiting for port, return
            while not self._port.writable():
                if not self._port.isOpen():
                    return

            log.debug(command)
            self._port.write(command)
            self._port.flush()

        while self._is_running:
            try:
                _send_next_command()
            except queue.Empty:
                continue

        # finalize queue
        while self._command_queue.qsize() > 0:
            try:
                _send_next_command()
            except queue.Empty:
                break

    def _send_command(self, command_name, *args):
        if len(args) > 0:
            self._send_command_raw(f'{command_name} {",".join(map(str, args))}')
        else:
            self._send_command_raw(command_name)

    def _send_command_raw(self, command: str):
        if not self._is_running: return
        self._command_queue.put(command.encode("ascii") + b'\n')

    def reset(self):
        self._send_command("r")

    def send(self, pin=0):
        self._send_command("st", pin)

    def wait(self, milliseconds):
        self._send_command("w", milliseconds)
        time.sleep(milliseconds * 0.001)

    def add_position(self, azimuth, altitude):
        if azimuth != 0: self.add_azimuth(azimuth)
        if altitude != 0: self.add_altitude(altitude)

    def add_azimuth(self, azimuth):
        self._send_command("az", azimuth)

    def add_altitude(self, altitude):
        self._send_command("al", altitude)
