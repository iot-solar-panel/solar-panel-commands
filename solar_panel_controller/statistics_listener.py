import csv
import logging
import threading
from datetime import datetime
from pathlib import Path

import serial
import time
from serial import SerialException

from statistics_parsing.sample_batch import SampleBatch
from statistics_parsing.statistics_parser import StatisticsParser

log = logging.getLogger(__name__)


class StatisticsListener(threading.Thread):
    baud_rate = 9600

    def __init__(self, save_folder=Path("./statistics")):
        super().__init__()
        self._save_path = self.get_save_file_path(save_folder)
        self._port = None
        self._batch = SampleBatch()
        self._is_running = False

    def start(self):
        self._is_running = True
        super().start()

    def join(self, **kwargs):
        self._is_running = False
        super().join(**kwargs)

    def set_port(self, port):
        self._port = port

    @staticmethod
    def get_save_file_path(root_folder):
        timestamp_str = datetime.now().strftime('%H-%M-%S-%fD%d-%m-%Y')
        file_path = Path(f"statistics_{timestamp_str}.csv")
        file_path = root_folder / file_path

        if not file_path.parent.exists():
            file_path.parent.mkdir(parents=True)
        file_path.touch(exist_ok=True)
        return file_path

    def _append_records(self, records):
        with self._save_path.open('a') as f:
            writer = csv.writer(f, dialect="excel")
            for record in records:
                log.info(f"read {record}")
                self._batch.add_record(record)
                writer.writerow((self._batch.timestamp, *record.to_tuple()))

    def _write_headers(self):
        if not self._save_path.exists():
            with self._save_path.open("w+") as f:
                writer = csv.writer(f, dialect="excel")
                writer.writerow(("time", "azimuth", "altitude", "voltage", "power"))

    def run(self):
        if self._port is None:
            log.error("port was not set before listening started")
            return

        log.info(f"listening to port {self._port.port}")
        self._batch.timestamp = time.time()

        self._write_headers()
        parser = StatisticsParser()
        while self._port.isOpen() and self._is_running:
            try:
                data_chunk = self._port.read(100)
            except SerialException:
                log.warning(f"cannot read port")
                break

            if len(data_chunk) > 0:
                parser.add_data(data_chunk)
                parser.parse()
                if not parser.ready(): continue
                self._append_records(parser.pop_results())

        log.debug(f"listening finished, path: {self._save_path}")
