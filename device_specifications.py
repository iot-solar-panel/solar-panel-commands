import os


_max_voltage = 5
_analog_resolution = 1024

voltage_per_unit = _max_voltage / _analog_resolution

load_resistance = 82
