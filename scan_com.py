import logging

import serial
from serial.tools import list_ports, list_ports_common

log = logging.getLogger(__name__)


def find_by_description(substring):
    port: list_ports_common.ListPortInfo
    for port in list_ports.comports():
        log.debug(port)
        if substring in port.description:
            log.info(f"found port [{port.device}]: {port.description}")
            return port.device
    return None


def find_arduino():
    return find_by_description("Arduino")


def find_usb_uart():
    return find_by_description("UART")


if __name__ == '__main__':
    print(find_usb_uart())
