import os
import unittest
from datetime import datetime

os.environ.setdefault("NUMBA_DISABLE_JIT", "1")
from sun_position.sunposition import observed_sunpos


class TestSunPosition(unittest.TestCase):

    def test_position(self):
        latitude = 51.38953418301934
        longitude = 30.099182004169016
        elevation = 110

        timestamp = 1651154258.0377498
        now = datetime.fromtimestamp(timestamp)
        azimuth, zenith = observed_sunpos(now, latitude, longitude, elevation)

        self.assertAlmostEqual(azimuth, 290.61637493890476)
        self.assertAlmostEqual(zenith, 87.71498994738155)
